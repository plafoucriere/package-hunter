'use strict'

const Docker = require('dockerode')
const debug = require('debug')('pkgs:docker-manager')
const path = require('path')
const { spawnHelper } = require('./util')
const JobTimeoutError = require('./job-timeout-error')

const IMAGE_NAME = 'maldep' // IMPORTANT: when changing the image name, the name in the rules (`falco/falco_rules.local.yaml`) must be updated to match the new name
const CONTAINER_NAME = 'some-container'
const CONTAINER_PKG_PATH = '/tmp/packages'

class DockerManager {
  constructor (packagePath, opts) {
    this.docker = new Docker(opts && opts.dockerodeConfig)
    this.packagePath = packagePath
  }

  async _build () {
  // ToDo: Build image manually for now, maybe automate in future
  }

  async _create (target, createOpts = {}) {
    // Valid container names are [a-zA-Z0-9][a-zA-Z0-9_.-]*
    // Filter out other characters.
    // Ex. `some-container-hash!-0.0.0.tgz` -> 'some-container-hash-0.0.0.tgz'
    const name = (CONTAINER_NAME + '-' + target).replace(/[^a-zA-Z0-9_.-]*/g, '')
    debug(`creating container ${name} of image ${IMAGE_NAME}`)

    const defaultCreateOpts = {
      Cmd: ['npm', 'install', path.join(CONTAINER_PKG_PATH, target)],
      Env: ['NO_UPDATE_NOTIFIER=1'], // supress the update notifier when starting npm
      Image: IMAGE_NAME,
      name: name,
      Tty: true,
      HostConfig: {
        AutoRemove: true,
        Mounts: [{
          Target: CONTAINER_PKG_PATH,
          Source: this.packagePath,
          Type: 'bind',
          ReadOnly: true
        }]
      }
    }
    this.container = await this.docker.createContainer(Object.assign({}, defaultCreateOpts, createOpts))

    debug(`container ${this.container.id} created`)

    return this.container.id
  }

  // copies file to path in this.container
  async _cpIntoContainer (srcPath, dstPath) {
    // dockerode unfortunately doesn't support docker cp
    // use docker cli tool instead
    debug(`copy ${srcPath} into container ${this.container.id}:${dstPath}`)
    try {
      await spawnHelper('docker', ['cp', srcPath, this.container.id + ':' + dstPath])
    } catch (err) {
      debug('failed to copy into container: ', err)
      throw err
    }
  }

  async _start (opts = { jobTimeout: 0 }) {
    if (opts.jobTimeout) { await this._startWithTimeout(opts.jobTimeout) } else { await this._startHelper() }
  }

  async _startWithTimeout (timeout) {
    if (!timeout || !(typeof (timeout) === 'number') || timeout <= 0) throw new TypeError('timeout must be an integer greater than 0')

    let id
    const timeoutPromise = new Promise((resolve, reject) => {
      id = setTimeout(() => {
        reject(new JobTimeoutError(`execution of container ${this.container.id} timed out`))
      }
      , timeout * 1000)
    })

    return Promise.race([this._start(), timeoutPromise]).then(() => clearTimeout(id))
  }

  async _startHelper () {
    debug(`starting container ${this.container.id}`)
    await this.container.start()
    const stream = await this.container.attach({
      stream: true,
      stdout: true,
      stderr: true
    })

    await this._follow(stream)
  }

  // t: -1: container has t seconds to complete the stop procedure before it is killed
  async _stop (opts = { t: -1 }) {
    if (!this.container) return // nothing to stop
    debug(`stoping container ${this.container.id}`)
    try {
      await this.container.stop(opts)
    } catch (err) {
      if (err.reason === 'container already stopped') return
      throw err
    }
  }

  async _remove () {
    if (!this.container) return // nothing to remove
    debug(`removing container ${this.container.id}`)
    try {
      await this.container.remove()
    } catch (err) {
      if (err.json && err.json.message &&
        err.json.message.indexOf('is already in progress') > -1) return
      throw err
    }
  }

  async _exec (tarball) {
    debug(`exec ${tarball} in container ${this.container.id}`)
    const exec = await this.container.exec({
      Cmd: ['npm', 'install', path.join(CONTAINER_PKG_PATH, tarball)],
      AttachStdout: true,
      AttachStderr: true,
      Tty: true
    })
    await exec.start()
  }

  async _follow (stream) {
    stream.on('data', (chunk) => {
      debug(chunk.toString('utf8'))
    })

    await new Promise((resolve, reject) => {
      debug(`stdout of ${this.container.id} closed`)
      stream.on('close', resolve)
    })
  }

  async cleanup () {
    try {
      await this._stop({ t: 0 })
      await this._remove()
    } catch (err) {
      if (err.reason && err.reason === 'no such container') return // noting to cle
      throw err
    }
  }
}

module.exports = DockerManager
